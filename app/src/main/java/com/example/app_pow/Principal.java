package com.example.app_pow;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;

public class Principal extends AppCompatActivity{


    TextView txtFome, txtSede, txtDiversao;
    ImageView statusChico;

    int fomeThread;

    int sedeThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        txtFome = findViewById(R.id.textViewFome);
        txtSede = findViewById(R.id.textViewSede);
        txtDiversao = findViewById(R.id.textViewDiversao);

        statusChico = findViewById(R.id.statusChico);

        thread.start();

    }

    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true){

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                fomeThread = Integer.parseInt(txtFome.getText().toString());
                sedeThread = Integer.parseInt(txtSede.getText().toString());

                fomeThread -= 5;
                sedeThread -= 5;

                txtFome.post(new Runnable() {
                    @Override
                    public void run() {
                        txtFome.setText("" + fomeThread);
                    }
                });

                txtSede.post(new Runnable() {
                    @Override
                    public void run() {
                        txtSede.setText(String.valueOf(sedeThread));
                    }
                });

            }
        }
    });

    public void DarComida(View view) {

        int fome = Integer.parseInt(txtFome.getText().toString());

        fome += 1;
        if(fome >= 100){

            fome = 100;
            txtFome.setText(""+fome);
        }else {

            txtFome.setText(""+fome);
        }
    }

    public void DarSuco(View view) {

        int sede = Integer.parseInt(txtSede.getText().toString());

        sede += 1;
        if(sede >= 100){

            sede = 100;
            txtSede.setText(""+sede);
        }else {

            txtSede.setText(""+sede);
        }
    }

    public void Divertir(View view) {

        int diversao = Integer.parseInt(txtDiversao.getText().toString());

        diversao += 1;
        if(diversao >= 100){

            diversao = 100;
            txtDiversao.setText(""+diversao);
        }else {

            txtDiversao.setText(""+diversao);
        }

        if(diversao >= 60){

            statusChico.setImageResource(R.mipmap.chico_feliz);
        }
    }

    public void DeixarBrabo(View view) {

        int diversao = Integer.parseInt(txtDiversao.getText().toString());

        diversao -= 50;

        if(diversao <= 0){

            diversao = 0;
            txtDiversao.setText(""+diversao);
        }else {

            txtDiversao.setText(""+diversao);
        }

        statusChico.setImageResource(R.mipmap.chico_triste);

    }
}
