package com.example.app_pow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Jogar(View view) {

        Intent i = new Intent(this,Principal.class);
        startActivity(i);
        finish();

    }

    public void Creditos(View view) {

        Intent i = new Intent(this,Creditos.class);
        startActivity(i);
        finish();
    }
}
